import 'package:flutter/material.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final nameController = TextEditingController();
  final surnameController = TextEditingController();
  final mobileController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final name = TextField(
      obscureText: false,
      controller: nameController,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: "Enter your FullNames",
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(30.0)),
      ),
    );

    final surname = TextField(
      obscureText: false,
      controller: surnameController,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: "Enter your LastName",
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(30.0)),
      ),
    );

    final mobile = TextField(
      obscureText: false,
      controller: mobileController,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: "Enter your Cell Number",
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(30.0)),
      ),
    );

    final email = TextField(
      obscureText: false,
      controller: emailController,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: "Enter your Email address",
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(30.0)),
      ),
    );

    final password = TextField(
      obscureText: true,
      controller: passwordController,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: "Enter your Password",
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(30.0)),
      ),
    );

    final registrationButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(20.0),
      color: Colors.blue[700],
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.fromLTRB(50.0, 15.0, 50.0, 15.0),
        onPressed: () {
          User user = User(
              nameController.text,
              surnameController.text,
              mobileController.text,
              emailController.text,
              passwordController.text);
          user.name;
          user.surname;
          user.mobile;
          user.email;
          user.password;

          showAlertDialog(context, user);
        },
        child: const Text(
          "Submit",
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
    return Scaffold(
      appBar: AppBar(title: const Text("Registration")),
      body: Center(
        child: Container(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(36.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Registration Form",
                  style: TextStyle(
                      color: Colors.blue[700],
                      fontSize: 30,
                      fontWeight: FontWeight.bold),
                ),
                const SizedBox(height: 25.0),
                name,
                const SizedBox(height: 25.0),
                surname,
                const SizedBox(height: 25.0),
                mobile,
                const SizedBox(height: 25.0),
                email,
                const SizedBox(height: 25.0),
                password,
                const SizedBox(height: 35.0),
                registrationButton,
                const SizedBox(height: 35.0),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

showAlertDialog(BuildContext context, User user) {
  Widget okButton = TextButton(
    child: const Text("GOT IT"),
    onPressed: () {
      Navigator.of(context, rootNavigator: true).pop("Alert");
    },
  );

  AlertDialog alert = AlertDialog(
    title: const Text("User Information"),
    content: Text(user.name +
        "\n" +
        user.surname +
        "\n" +
        user.mobile +
        "\n" +
        user.email +
        "\n" +
        user.password),
    actions: [okButton],
  );

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

class User {
  //Declaring Variables

  String name = " ";
  String surname = " ";
  String mobile = " ";
  String email = " ";
  String password = " ";

  User(String name, String surname, String mobile, String email,
      String password) {
    name = this.name;
    surname = this.surname;
    mobile = this.mobile;
    email = this.email;
    password = this.password;
  }

//Getters

  String get getname {
    return name;
  }

  String get getsurname {
    return surname;
  }

  String get getmobile {
    return mobile;
  }

  String get getemail {
    return email;
  }

  String get getpassword {
    return password;
  }

  //Setters

  set setName(String name) {
    this.name = name;
  }

  set setSurname(String surname) {
    this.surname = surname;
  }

  set setMobile(String mobile) {
    this.mobile = mobile;
  }

  set setEmail(String email) {
    this.email = email;
  }

  set setPassword(String password) {
    this.password = password;
  }
}

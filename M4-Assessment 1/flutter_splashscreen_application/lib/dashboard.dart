import 'package:flutter/material.dart';
import 'login_screen.dart';
import 'user_profile.dart';
import 'images.dart';
import 'contact.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Mavellicious Dashboard"),
        backgroundColor: Colors.blue[700],
      ),
      backgroundColor: Colors.blue[100],
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: const Text(
                "Mavellicious App Admin",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17.0),
              ),
              accountEmail: const Text("admin@yahoo.com"),
              currentAccountPicture: const CircleAvatar(
                backgroundImage: NetworkImage(
                    "https://media.istockphoto.com/photos/soccer-ball-picture-id482855855"),
              ),
              decoration: BoxDecoration(color: Colors.blue[700]),
            ),
            ListTile(
              leading: const Icon(Icons.person),
              title: const Text("User Editing Profile"),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const EditUserProfile()),
                );
              },
            ),
            ListTile(
              leading: const Icon(Icons.contact_page),
              title: const Text("Contacts"),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const MyContacts()),
                );
              },
            ),
            ListTile(
              leading: const Icon(Icons.image),
              title: const Text("Images"),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const Images()),
                );
              },
            ),
            const ListTile(
              leading: Icon(Icons.info),
              title: Text("Information"),
            ),
            ListTile(
              leading: const Icon(Icons.exit_to_app),
              title: const Text("LogOut"),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const LoginScreen()),
                );
              },
            ),
          ],
        ),
      ),
      body: Container(
        padding: const EdgeInsets.all(20.0),
        child: GridView.count(
          crossAxisCount: 2,
          children: const <Widget>[
            MyMenu(
              title: "Home-Page",
              icon: Icons.home_rounded,
              warna: Colors.blue,
            ),
            MyMenu(
              title: "User Editing Profile",
              icon: Icons.person,
              warna: Colors.blue,
            ),
            MyMenu(
              title: "Contacts",
              icon: Icons.contact_page,
              warna: Colors.blue,
            ),
            MyMenu(
              title: "Images",
              icon: Icons.image,
              warna: Colors.blue,
            ),
          ],
        ),
      ),
    );
  }
}

class MyMenu extends StatelessWidget {
  const MyMenu(
      {Key? key, required this.title, required this.icon, required this.warna})
      : super(key: key);

  final String title;
  final IconData icon;
  final MaterialColor warna;

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.all(8.0),
      child: InkWell(
        onTap: () {
          Navigator.of(context).pushNamed("yes");
        },
        splashColor: Colors.blue,
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Icon(
                icon,
                size: 70.0,
                color: warna,
              ),
              Text(title, style: const TextStyle(fontSize: 17.0))
            ],
          ),
        ),
      ),
    );
  }
}
